using System.Diagnostics.CodeAnalysis;
using CSharpFunctionalExtensions;

namespace BasketApp.Core.Domain.Model.BasketAggregate;

/// <summary>
///     Статус корзины
/// </summary>
public class Status : ValueObject
{
    public static Status Created => new(nameof(Created).ToLowerInvariant());
    public static Status Confirmed => new(nameof(Confirmed).ToLowerInvariant());

    /// <summary>
    ///     Ctr
    /// </summary>
    [ExcludeFromCodeCoverage]
    private Status()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="name">Название</param>
    private Status(string name) : this()
    {
        Name = name;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; private set;}

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Name;
    }
}