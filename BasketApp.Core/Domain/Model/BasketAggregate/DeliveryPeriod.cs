﻿using System.Diagnostics.CodeAnalysis;
using CSharpFunctionalExtensions;
using Primitives;

namespace BasketApp.Core.Domain.Model.BasketAggregate;

/// <summary>
///     Период доставки
/// </summary>
public class DeliveryPeriod : Entity<int>
{
    public static readonly DeliveryPeriod Morning = new(1, nameof(Morning).ToLowerInvariant(), 6, 12);
    public static readonly DeliveryPeriod Midday = new(2, nameof(Midday).ToLowerInvariant(), 12, 17);
    public static readonly DeliveryPeriod Evening = new(3, nameof(Evening).ToLowerInvariant(), 17, 24);
    public static readonly DeliveryPeriod Night = new(4, nameof(Night).ToLowerInvariant(), 0, 6);

    /// <summary>
    ///     Ctr
    /// </summary>
    [ExcludeFromCodeCoverage]
    private DeliveryPeriod()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="name">Название</param>
    /// <param name="from">Начало периода, часы</param>
    /// <param name="to">Конец периода, часы</param>
    private DeliveryPeriod(int id, string name, int from, int to) : this()
    {
        Id = id;
        Name = name;
        From = from;
        To = to;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    /// <summary>
    ///     Начало периода
    /// </summary>
    public int From { get; private set; }

    /// <summary>
    ///     Конец периода
    /// </summary>
    public int To { get; private set; }

    /// <summary>
    ///     Список всех значений списка
    /// </summary>
    /// <returns>Значения списка</returns>
    public static IEnumerable<DeliveryPeriod> List()
    {
        yield return Morning;
        yield return Midday;
        yield return Evening;
        yield return Night;
    }

    /// <summary>
    ///     Получить период доставки по названию
    /// </summary>
    /// <param name="name">Название</param>
    /// <returns>Период доставки</returns>
    public static Result<DeliveryPeriod, Error> FromName(string name)
    {
        var state = List()
            .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
        if (state == null) return Errors.DeliveryPeriodIsWrong();
        return state;
    }

    /// <summary>
    ///     Получить период доставки по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Период доставки</returns>
    public static Result<DeliveryPeriod, Error> FromId(int id)
    {
        var state = List().SingleOrDefault(s => s.Id == id);
        if (state == null) return Errors.DeliveryPeriodIsWrong();
        return state;
    }

    /// <summary>
    ///     Ошибки, которые может возвращать сущность
    /// </summary>
    public static class Errors
    {
        public static Error DeliveryPeriodIsWrong()
        {
            return new Error($"{nameof(DeliveryPeriod).ToLowerInvariant()}.is.wrong",
                $"Не верное значение. Допустимые значения: {nameof(DeliveryPeriod).ToLowerInvariant()}: {string.Join(",", List().Select(s => s.Name))}");
        }
    }
}