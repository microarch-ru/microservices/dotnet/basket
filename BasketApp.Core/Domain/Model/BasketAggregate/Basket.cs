﻿using System.Diagnostics.CodeAnalysis;
using BasketApp.Core.Domain.Model.BasketAggregate.DomainEvents;
using BasketApp.Core.Domain.Model.GoodAggregate;
using BasketApp.Core.Domain.Model.SharedKernel;
using CSharpFunctionalExtensions;
using Primitives;

namespace BasketApp.Core.Domain.Model.BasketAggregate;

/// <summary>
///     Корзина
/// </summary>
public class Basket : Aggregate<Guid>
{
    /// <summary>
    ///     Ctr
    /// </summary>
    [ExcludeFromCodeCoverage]
    private Basket()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="bayerId">Идентификатор покупателя</param>
    private Basket(Guid bayerId) : this()
    {
        Id = Guid.NewGuid();
        BayerId= bayerId;
        Status = Status.Created;
    }

    /// <summary>
    /// Идентификатор покупателя
    /// </summary>
    public Guid BayerId { get; private set; }
    
    /// <summary>
    ///     Адрес доставки
    /// </summary>
    public Address Address { get; private set; }

    /// <summary>
    ///     Период доставки
    /// </summary>
    public DeliveryPeriod DeliveryPeriod { get; private set; }

    /// <summary>
    ///     Товарные позиции
    /// </summary>
    public List<Item> Items { get; private set; } = new();

    /// <summary>
    ///     Статус
    /// </summary>
    public Status Status { get; private set; }

    /// <summary>
    ///     Итоговая стоимость корзины, учитывая скидку
    /// </summary>
    public decimal Total { get; private set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="buyerId">Идентификатор покупателя</param>
    /// <returns>Результат</returns>
    public static Result<Basket, Error> Create(Guid buyerId)
    {
        if (buyerId == Guid.Empty) return GeneralErrors.ValueIsRequired(nameof(buyerId));
        return new Basket(buyerId);
    }

    /// <summary>
    ///     Изменить позицию
    /// </summary>
    /// <param name="good">Товар</param>
    /// <param name="quantity">Количество</param>
    /// <returns>Результат</returns>
    public UnitResult<Error> Change(Good good, int quantity)
    {
        if (Status == Status.Confirmed) return Errors.BasketHasAlreadyBeenIssued();
        if (good == null) return GeneralErrors.ValueIsRequired(nameof(good));
        if (quantity < 0) return GeneralErrors.ValueIsInvalid(nameof(quantity));

        var item = Items.SingleOrDefault(x => x.GoodId == good.Id);
        if (item != null)
        {
            if (quantity == 0)
                Items.Remove(item);
            else
                item.SetQuantity(quantity);
        }
        else
        {
            var result = Item.Create(good, quantity);
            if (result.IsFailure) return result.Error;

            Items.Add(result.Value);
        }
        
        // Рассчитываем итоговую стоимость всех товаров в корзине
        Total = Items.Sum(o => o.GetSubTotal().Value);
        
        return UnitResult.Success<Error>();
    }

    /// <summary>
    ///     Добавить адрес доставки
    /// </summary>
    /// <param name="address">Адрес доставки</param>
    /// <returns>Результат</returns>
    public UnitResult<Error> AddAddress(Address address)
    {
        if (Status == Status.Confirmed) return Errors.BasketHasAlreadyBeenIssued();
        Address = address;
        return UnitResult.Success<Error>();
    }

    /// <summary>
    ///     Добавить период доставки
    /// </summary>
    /// <param name="deliveryPeriod">Период доставки</param>
    /// <returns>Результат</returns>
    public UnitResult<Error> AddDeliveryPeriod(DeliveryPeriod deliveryPeriod)
    {
        if (Status == Status.Confirmed) return Errors.BasketHasAlreadyBeenIssued();
        DeliveryPeriod = deliveryPeriod;
        return UnitResult.Success<Error>();
    }

    /// <summary>
    ///     Оформить корзину
    /// </summary>
    /// <param name="discount">Скидка</param>
    /// <returns>Результат</returns>
    public UnitResult<Error> Checkout(Discount discount)
    {
        if (Status == Status.Confirmed) return Errors.BasketHasAlreadyBeenIssued();
        if (Items.Count <= 0) return GeneralErrors.InvalidLength(nameof(Items));
        if (Address == null) return GeneralErrors.ValueIsRequired(nameof(Address));
        if (DeliveryPeriod == null) return GeneralErrors.ValueIsRequired(nameof(DeliveryPeriod));
        if (discount == null) return GeneralErrors.ValueIsRequired(nameof(discount));

        // Рассчитываем итоговую стоимость, учитывая размер скидки
        var sum = Items.Sum(o => o.GetSubTotal().Value);
        Total = sum - sum * (decimal)discount.Value;
        
        // Меняем статус
        Status = Status.Confirmed;
        
        // Публикуем доменное событие
        RaiseDomainEvent(new BasketConfirmedDomainEvent(this));
        return UnitResult.Success<Error>();
    }

    public static class Errors
    {
        public static Error BasketHasAlreadyBeenIssued()
        {
            return new Error($"{nameof(Basket).ToLowerInvariant()}.has.already.been.issued", "Корзина уже оформлена");
        }
    }
}