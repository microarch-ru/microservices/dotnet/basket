using Primitives;

namespace BasketApp.Core.Domain.Model.BasketAggregate.DomainEvents;

public sealed record BasketConfirmedDomainEvent(Basket Basket) : DomainEvent;