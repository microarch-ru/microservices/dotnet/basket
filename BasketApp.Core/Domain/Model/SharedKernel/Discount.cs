using System.Diagnostics.CodeAnalysis;
using CSharpFunctionalExtensions;
using Primitives;

namespace BasketApp.Core.Domain.Model.SharedKernel;

/// <summary>
///     Вес
/// </summary>
public class Discount : ValueObject
{
    public static Discount None => new(0);
    
    /// <summary>
    ///     Ctr
    /// </summary>
    [ExcludeFromCodeCoverage]
    private Discount()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="value">Значение</param>
    private Discount(double value) : this()
    {
        Value = value;
    }

    /// <summary>
    ///     Значение
    /// </summary>
    public double Value { get; private set;}

    /// <summary>
    ///     Создание
    /// </summary>
    /// <param name="value">Значение</param>
    /// <returns>Результат</returns>
    public static Result<Discount, Error> Create(double value)
    {
        if (value < 0) return GeneralErrors.ValueIsRequired(nameof(value));
        if (value > 1) return GeneralErrors.ValueIsRequired(nameof(value));
        return new Discount(value);
    }

    /// <summary>
    ///     Сравнить две скидки
    /// </summary>
    /// <param name="first">Скидка 1</param>
    /// <param name="second">Скидка 2</param>
    /// <returns>Результат</returns>
    public static bool operator <(Discount first, Discount second)
    {
        var result = first.Value < second.Value;
        return result;
    }

    /// <summary>
    ///     Сравнить два скидки
    /// </summary>
    /// <param name="first">Скидка 1</param>
    /// <param name="second">Скидка 2</param>
    /// <returns>Результат</returns>
    public static bool operator >(Discount first, Discount second)
    {
        var result = first.Value > second.Value;
        return result;
    }

    /// <summary>
    ///     Перегрузка для определения идентичности
    /// </summary>
    /// <returns>Результат</returns>
    /// <remarks>Идентичность будет происходить по совокупности полей указанных в методе</remarks>
    [ExcludeFromCodeCoverage]
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }
}