using System.Diagnostics.CodeAnalysis;
using BasketApp.Core.Domain.Model.SharedKernel;
using CSharpFunctionalExtensions;
using Primitives;

namespace BasketApp.Core.Domain.Model.GoodAggregate;

/// <summary>
///     Товар
/// </summary>
public class Good : Aggregate<Guid>
{
    /// <summary>
    ///     Хлеб
    /// </summary>
    public static Good Bread => new(new Guid("ec85ceee-f186-4e9c-a4dd-2929e69e586c")
        , "Хлеб"
        , "Описание хлеба"
        , 100
        , 10
        , Weight.Create(6).Value);

    /// <summary>
    ///     Молоко
    /// </summary>
    public static Good Milk => new(new Guid("e8cb8a0b-d302-485a-801c-5fb50aced4d5")
        , "Молоко"
        , "Описание молока"
        , 200
        , 10
        , Weight.Create(9).Value);

    /// <summary>
    ///     Яйца
    /// </summary>
    public static Good Eggs => new(new Guid("a1d48be9-4c98-4371-97c0-064bde03874e")
        , "Яйца"
        , "Описание яиц"
        , 300
        , 10
        , Weight.Create(8).Value);

    /// <summary>
    ///     Колбаса
    /// </summary>
    public static Good Sausage => new(new Guid("34b1e64a-6471-44a0-8c4a-e5d21584a76c")
        , "Колбаса"
        , "Описание колбасы"
        , 400
        , 10
        , Weight.Create(4).Value);

    /// <summary>
    ///     Кофе
    /// </summary>
    public static Good Coffee => new(new Guid("292dc3c5-2bdd-4e0c-bd75-c5e8b07a8792")
        , "Кофе"
        , "Описание кофе"
        , 500
        , 10
        , Weight.Create(7).Value);

    public static Good Sugar => new(new Guid("a3fcc8e1-d2a3-4bd6-9421-c82019e21c2d")
        , "Сахар",
        "Описание сахара"
        , 600
        , 10
        , Weight.Create(1).Value);
    
    public static Good Gum => new(new Guid("3ecc7f05-7081-4155-9ca1-a3e371faa661")
        , "Жвачка",
        "Промо товар"
        , 1
        , 10
        , Weight.Create(1).Value);
    
    public static Good Candy => new(new Guid("67dda004-f324-4868-af9a-88f77d1d28fc")
        , "Конфета",
        "Промо товар"
        , 1
        , 10
        , Weight.Create(1).Value);
    
    public static Good Snack => new(new Guid("5399a4c3-8b32-418a-b477-cedb6be815d0")
        , "Перекус",
        "Промо товар"
        , 1
        , 10
        , Weight.Create(1).Value);
    

    /// <summary>
    ///     Ctr
    /// </summary>
    [ExcludeFromCodeCoverage]
    private Good()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="title">Название</param>
    /// <param name="description">Описание</param>
    /// <param name="price">Цена</param>
    /// <param name="quantity">Количество (остатки)</param>
    /// <param name="weight">Вес</param>
    private Good(Guid id, string title, string description, decimal price, int quantity, Weight weight) : this()
    {
        Id = id;
        Title = title;
        Description = description;
        Price = price;
        Quantity = quantity;
        Weight = weight;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Title { get; private set;}

    /// <summary>
    ///     Описание
    /// </summary>
    public string Description { get; private set;}

    /// <summary>
    ///     Стоимость
    /// </summary>
    public decimal Price { get; private set; }

    /// <summary>
    ///     Вес
    /// </summary>
    public Weight Weight { get; private set;}

    /// <summary>
    ///     Количество (остатки)
    /// </summary>
    public int Quantity { get; private set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="title">Название</param>
    /// <param name="description">Описание</param>
    /// <param name="price">Цена</param>
    /// <param name="quantity">Количество (остатки)</param>
    /// <param name="weight">Вес</param>
    /// <returns>Результат</returns>
    public static Result<Good, Error> Create(string title, string description, decimal price, int quantity,
        Weight weight)
    {
        var id = Guid.NewGuid();
        if (string.IsNullOrEmpty(title)) GeneralErrors.ValueIsInvalid(nameof(title));
        if (string.IsNullOrEmpty(description)) GeneralErrors.ValueIsRequired(nameof(description));
        if (price <= 0) GeneralErrors.ValueIsRequired(nameof(price));
        if (quantity <= 0) GeneralErrors.ValueIsRequired(nameof(price));
        if (weight == null) GeneralErrors.ValueIsRequired(nameof(weight));

        return new Good(id, title, description, price, quantity, weight);
    }

    /// <summary>
    ///     Изменить остатки товара
    /// </summary>
    /// <param name="quantity">Количество</param>
    /// <returns></returns>
    public UnitResult<Error> ChangeStocks(int quantity)
    {
        if (quantity < 0) GeneralErrors.ValueIsRequired(nameof(quantity));

        Quantity = quantity;
        return UnitResult.Success<Error>();
    }

    public static IEnumerable<Good> List()
    {
        yield return Bread;
        yield return Milk;
        yield return Eggs;
        yield return Sausage;
        yield return Coffee;
        yield return Sugar;
    }
}