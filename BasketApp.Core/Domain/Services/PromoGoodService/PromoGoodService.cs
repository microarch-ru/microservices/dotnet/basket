using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Domain.Model.GoodAggregate;
using CSharpFunctionalExtensions;
using Primitives;

namespace BasketApp.Core.Domain.Services.PromoGoodService;

/// <summary>
///     Добавляет в корзину промо товары, в зависимости от ее стоимости
/// </summary>
/// <remarks>Доменный сервис</remarks>
public class PromoGoodService : IPromoGoodService
{
    /// <summary>
    ///     Добавить промо товар в корзину
    /// </summary>
    /// <returns>Результат</returns>
    public UnitResult<Error> AddPromo(Basket basket)
    {
        if (basket == null) return GeneralErrors.ValueIsRequired(nameof(basket));

        var promoGum = Good.Gum;
        var promoCandy = Good.Candy;
        var promoSnack = Good.Snack;

        switch (basket.Total)
        {
            case > 1000 and <= 2000:
                basket.Change(promoGum, 1);
                break;
            case > 2000 and <= 5000:
                basket.Change(promoCandy,1);
                break;
            case > 5000:
                basket.Change(promoSnack,1);
                break;
        }
        return UnitResult.Success<Error>();
    }
}