using BasketApp.Core.Domain.Model.BasketAggregate;
using CSharpFunctionalExtensions;
using Primitives;

namespace BasketApp.Core.Domain.Services.PromoGoodService;

/// <summary>
///     Добавляет в корзину промо товары, в зависимости от ее стоимости
/// </summary>
/// <remarks>Доменный сервис</remarks>
public interface IPromoGoodService
{
    /// <summary>
    ///     Добавить промо товар в корзину
    /// </summary>
    /// <returns>Результат</returns>
    public UnitResult<Error> AddPromo(Basket basket);
}