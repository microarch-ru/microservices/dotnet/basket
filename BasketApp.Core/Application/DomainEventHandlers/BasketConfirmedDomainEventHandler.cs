using BasketApp.Core.Domain.Model.BasketAggregate.DomainEvents;
using BasketApp.Core.Ports;
using MediatR;

namespace BasketApp.Core.Application.DomainEventHandlers;

public sealed class BasketConfirmedDomainEventHandler : INotificationHandler<BasketConfirmedDomainEvent>
{
    private readonly IMessageBusProducer _messageBusProducer;

    public BasketConfirmedDomainEventHandler(IMessageBusProducer messageBusProducer)
    {
        _messageBusProducer = messageBusProducer;
    }

    public async Task Handle(BasketConfirmedDomainEvent notification, CancellationToken cancellationToken)
    {
        await _messageBusProducer.Publish(notification, cancellationToken);
    }
}