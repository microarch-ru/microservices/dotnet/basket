﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Ports;
using MediatR;
using Primitives;

namespace BasketApp.Core.Application.UseCases.Commands.ChangeItems;

public class ChangeItemsHandler : IRequestHandler<ChangeItemsCommand, bool>
{
    private readonly IBasketRepository _basketRepository;
    private readonly IGoodRepository _goodRepository;
    private readonly IUnitOfWork _unitOfWork;

    /// <summary>
    ///     Ctr
    /// </summary>
    public ChangeItemsHandler(IUnitOfWork unitOfWork, IBasketRepository basketRepository,
        IGoodRepository goodRepository)
    {
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
        _goodRepository = goodRepository ?? throw new ArgumentNullException(nameof(goodRepository));
    }

    public async Task<bool> Handle(ChangeItemsCommand message, CancellationToken cancellationToken)
    {
        // Нашли товар
        var good = await _goodRepository.GetAsync(message.GoodId);
        if (good == null) return false;

        // Восстанавливаем агрегат, если нет, то создали
        var basket = await _basketRepository.GetAsync(message.BasketId);
        if (basket == null)
        {
            // Создали агрегат
            var result = Basket.Create(message.BasketId);
            if (result.IsFailure) return false;
            basket = result.Value;
            
            // Изменили агрегат
            basket.Change(good, message.Quantity);

            // Сохранили агрегат
            await _basketRepository.AddAsync(basket);
        }
        else
        {
            // Изменили агрегат
            basket.Change(good, message.Quantity);
            _basketRepository.Update(basket);
        }
        
        // Сохранили агрегат
        return await _unitOfWork.SaveChangesAsync(cancellationToken);
    }
}