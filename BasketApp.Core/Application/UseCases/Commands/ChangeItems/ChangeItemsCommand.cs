﻿using MediatR;

namespace BasketApp.Core.Application.UseCases.Commands.ChangeItems;

/// <summary>
///     Изменить состав корзины
/// </summary>
public class ChangeItemsCommand : IRequest<bool>
{
    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="basketId">Идентификатор покупателя</param>
    /// <param name="goodId">Идентификатор товара</param>
    /// <param name="quantity">Количество позиций</param>
    public ChangeItemsCommand(Guid basketId, Guid goodId, int quantity)
    {
        if (basketId == Guid.Empty) throw new ArgumentException(nameof(basketId));
        if (goodId == Guid.Empty) throw new ArgumentException(nameof(goodId));
        if (quantity < 0) throw new ArgumentException(nameof(quantity));

        BasketId = basketId;
        GoodId = goodId;
        Quantity = quantity;
    }

    /// <summary>
    ///     Идентификатор покупателя
    /// </summary>
    public Guid BasketId { get; }

    /// <summary>
    ///     Идентификатор товара
    /// </summary>
    public Guid GoodId { get; }

    /// <summary>
    ///     Количество позиций
    /// </summary>
    public int Quantity { get; }
}