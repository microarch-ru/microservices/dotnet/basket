﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Domain.Model.GoodAggregate;
using BasketApp.Core.Domain.Model.SharedKernel;
using BasketApp.Core.Ports;
using CSharpFunctionalExtensions;
using MediatR;
using Primitives;

namespace BasketApp.Core.Application.UseCases.Commands.TestCheckout;

public class TestCheckoutHandler : IRequestHandler<TestCheckoutCommand, bool>
{
    private readonly IBasketRepository _basketRepository;
    private readonly IDiscountClient _discountClient;
    private readonly IUnitOfWork _unitOfWork;

    public TestCheckoutHandler(IUnitOfWork unitOfWork, IBasketRepository basketRepository,
        IDiscountClient discountClient)
    {
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
        _discountClient = discountClient ?? throw new ArgumentNullException(nameof(discountClient));
    }

    /// <summary>
    ///     Эта команда исключительно для тестирования!
    ///     Мы в ней не боролись за красоту и правильность.
    /// </summary>
    public async Task<bool> Handle(TestCheckoutCommand message, CancellationToken cancellationToken)
    {
        var rnd = new Random(Guid.NewGuid().GetHashCode());

        // Создаем корзину и наполняем товарами
        var basketId = Guid.NewGuid();
        var result = Basket.Create(basketId);
        if (result.IsFailure) return false;
        var basket = result.Value;

        // Добавляем товары в корзину
        var randomItems = rnd.Next(1, 10);
        for (var i = 0; i < randomItems; i++)
        {
            // Выбираем рандомный товар
            var goods = Good.List().ToList();
            var randomGoodIndex = rnd.Next(goods.Count() - 1);
            var randomGood = goods[randomGoodIndex];

            // Выбираем рандомной количество
            var randomQuantity = rnd.Next(1, 10);

            // Добавляем товар в корзину
            basket.Change(randomGood, randomQuantity);
        }

        // Указываем адрес
        // var streets = new List<string>
        // {
        //     "Тестировочная", "Айтишная", "Эйчарная", "Аналитическая", "Нагрузочная",
        //     "Серверная", "Мобильная", "Бажная"
        // };
        //var randomAddressIndex = rnd.Next(streets.Count() - 1);
        var addressCreateResult = Address.Create("Россия",
            "Москва", "Несуществующая", "1", "1");
        if (result.IsFailure) return false;
        var address = addressCreateResult.Value;
        basket.AddAddress(address);

        // Указываем период доставки
        var deliveryPeriods = DeliveryPeriod.List().ToList();
        var randomDeliveryPeriodIndex = rnd.Next(deliveryPeriods.Count() - 1);
        var randomDeliveryPeriod = deliveryPeriods[randomDeliveryPeriodIndex];
        basket.AddDeliveryPeriod(randomDeliveryPeriod);

        // Забираем скидку из сервиса Discount
        var (_, isFailure, discount) = await _discountClient.GetDiscountAsync(basket, cancellationToken);
        if (isFailure) return false;

        // Изменяем агрегат
        var basketCheckoutResult = basket.Checkout(discount);
        if (basketCheckoutResult.IsFailure) return false;

        //Сохраняем агрегат
        await _basketRepository.AddAsync(basket);
        return await _unitOfWork.SaveChangesAsync(cancellationToken);
    }
}