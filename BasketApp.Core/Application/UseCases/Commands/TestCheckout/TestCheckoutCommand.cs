﻿using MediatR;

namespace BasketApp.Core.Application.UseCases.Commands.TestCheckout;

/// <summary>
///     Оформить корзину
/// </summary>
public class TestCheckoutCommand : IRequest<bool>
{
    /// <summary>
    ///     Ctr
    /// </summary>
    public TestCheckoutCommand()
    {
    }
}