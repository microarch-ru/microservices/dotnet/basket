﻿using MediatR;

namespace BasketApp.Core.Application.UseCases.Commands.AddDeliveryPeriod;

/// <summary>
///     Добавить период доставки
/// </summary>
public class AddDeliveryPeriodCommand : IRequest<bool>
{
    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="basketId">Идентификатор корзины</param>
    /// <param name="deliveryPeriod">Период доставки</param>
    public AddDeliveryPeriodCommand(Guid basketId, string deliveryPeriod)
    {
        if (basketId == Guid.Empty) throw new ArgumentException(nameof(basketId));
        if (string.IsNullOrEmpty(deliveryPeriod)) throw new ArgumentException(nameof(DeliveryPeriod));

        BasketId = basketId;
        DeliveryPeriod = deliveryPeriod;
    }

    /// <summary>
    ///     Идентификатор корзины
    /// </summary>
    public Guid BasketId { get; }

    /// <summary>
    ///     Период доставки
    /// </summary>
    public string DeliveryPeriod { get; }
}