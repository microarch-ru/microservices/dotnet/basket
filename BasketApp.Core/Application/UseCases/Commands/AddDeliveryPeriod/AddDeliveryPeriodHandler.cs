﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Ports;
using MediatR;
using Primitives;

namespace BasketApp.Core.Application.UseCases.Commands.AddDeliveryPeriod;

public class AddDeliveryPeriodHandler : IRequestHandler<AddDeliveryPeriodCommand, bool>
{
    private readonly IBasketRepository _basketRepository;
    private readonly IUnitOfWork _unitOfWork;

    /// <summary>
    ///     Ctr
    /// </summary>
    public AddDeliveryPeriodHandler(IUnitOfWork unitOfWork, IBasketRepository basketRepository)
    {
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
    }

    public async Task<bool> Handle(AddDeliveryPeriodCommand message, CancellationToken cancellationToken)
    {
        //Восстанавливаем агрегат
        var basket = await _basketRepository.GetAsync(message.BasketId);

        //Изменяем агрегат
        var deliveryPeriodFromNameResult = DeliveryPeriod.FromName(message.DeliveryPeriod);
        if (deliveryPeriodFromNameResult.IsFailure) return false;

        var deliveryPeriod = deliveryPeriodFromNameResult.Value;
        var basketAddDeliveryPeriodResult = basket.AddDeliveryPeriod(deliveryPeriod);
        if (basketAddDeliveryPeriodResult.IsFailure) return false;

        //Сохраняем агрегат
        _basketRepository.Update(basket);
        return await _unitOfWork.SaveChangesAsync(cancellationToken);
    }
}