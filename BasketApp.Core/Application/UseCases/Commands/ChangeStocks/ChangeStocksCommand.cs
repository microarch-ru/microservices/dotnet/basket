﻿using MediatR;

namespace BasketApp.Core.Application.UseCases.Commands.ChangeStocks;

/// <summary>
///     Изменить остатки товара
/// </summary>
public class ChangeStocksCommand : IRequest<bool>
{
    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="goodId">Идентификатор товара</param>
    /// <param name="quantity">Остатки</param>
    public ChangeStocksCommand(Guid goodId, int quantity)
    {
        if (goodId == Guid.Empty) throw new ArgumentException(nameof(goodId));
        if (quantity <= 0) throw new ArgumentException(nameof(quantity));

        GoodId = goodId;
        Quantity = quantity;
    }

    /// <summary>
    ///     Идентификатор товара
    /// </summary>
    public Guid GoodId { get; }

    // <summary>
    /// Остатки
    /// </summary>
    public int Quantity { get; }
}