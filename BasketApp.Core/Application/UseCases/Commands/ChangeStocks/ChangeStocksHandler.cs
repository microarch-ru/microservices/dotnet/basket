﻿using BasketApp.Core.Ports;
using MediatR;
using Primitives;

namespace BasketApp.Core.Application.UseCases.Commands.ChangeStocks;

public class ChangeStocksHandler : IRequestHandler<ChangeStocksCommand, bool>
{
    private readonly IGoodRepository _goodRepository;
    private readonly IUnitOfWork _unitOfWork;

    /// <summary>
    ///     Ctr
    /// </summary>
    public ChangeStocksHandler(IUnitOfWork unitOfWork, IGoodRepository goodRepository)
    {
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _goodRepository = goodRepository ?? throw new ArgumentNullException(nameof(goodRepository));
    }

    public async Task<bool> Handle(ChangeStocksCommand message, CancellationToken cancellationToken)
    {
        // Восстанавливаем агрегат
        var good = await _goodRepository.GetAsync(message.GoodId);

        // Изменяем агрегат
        var goodChangeStocksResult = good.ChangeStocks(message.Quantity);
        if (goodChangeStocksResult.IsFailure) return false;

        // Сохраняем агрегат
        _goodRepository.Update(good);
        return await _unitOfWork.SaveChangesAsync(cancellationToken);
    }
}