﻿using BasketApp.Core.Domain.Services.PromoGoodService;
using BasketApp.Core.Ports;
using CSharpFunctionalExtensions;
using MediatR;
using Primitives;

namespace BasketApp.Core.Application.UseCases.Commands.Checkout;

public class CheckoutHandler : IRequestHandler<CheckoutCommand, UnitResult<Error>>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IBasketRepository _basketRepository;
    private readonly IPromoGoodService _promoGoodService;
    private readonly IDiscountClient _discountClient;

    public CheckoutHandler(IUnitOfWork unitOfWork, IBasketRepository basketRepository,
        IPromoGoodService promoGoodService, IDiscountClient discountClient)
    {
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
        _promoGoodService = promoGoodService ?? throw new ArgumentNullException(nameof(promoGoodService));
        _discountClient = discountClient ?? throw new ArgumentNullException(nameof(discountClient));
    }

    public async Task<UnitResult<Error>> Handle(CheckoutCommand message, CancellationToken cancellationToken)
    {
        // Восстанавливаем агрегат
        var basket = await _basketRepository.GetAsync(message.BasketId);
        if (basket == null) return GeneralErrors.NotFound();

        // Забираем скидку из сервиса Discount
        var getDiscountResult = await _discountClient.GetDiscountAsync(basket, cancellationToken);
        if (getDiscountResult.IsFailure) return getDiscountResult.Error;
        var discount = getDiscountResult.Value;

        // Добавляем промо товары
        _promoGoodService.AddPromo(basket);
        
        // Оформляем корзину
        var basketCheckoutResult = basket.Checkout(discount);
        if (basketCheckoutResult.IsFailure) return basketCheckoutResult.Error;

        // Сохраняем агрегат
        _basketRepository.Update(basket);
        await _unitOfWork.SaveChangesAsync(cancellationToken);
        return UnitResult.Success<Error>();
    }
}