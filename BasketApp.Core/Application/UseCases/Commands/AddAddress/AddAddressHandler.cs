﻿using BasketApp.Core.Domain.Model.SharedKernel;
using BasketApp.Core.Ports;
using MediatR;
using Primitives;

namespace BasketApp.Core.Application.UseCases.Commands.AddAddress;

public class AddAddressHandler : IRequestHandler<AddAddressCommand, bool>
{
    private readonly IBasketRepository _basketRepository;
    private readonly IUnitOfWork _unitOfWork;

    /// <summary>
    ///     Ctr
    /// </summary>
    public AddAddressHandler(IUnitOfWork unitOfWork, IBasketRepository basketRepository)
    {
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
    }

    public async Task<bool> Handle(AddAddressCommand message, CancellationToken cancellationToken)
    {
        //Восстанавливаем агрегат
        var basket = await _basketRepository.GetAsync(message.BasketId);

        //Изменяем агрегат
        var addressCreateResult =
            Address.Create(message.Country, message.City, message.Street, message.House, message.Apartment);
        if (addressCreateResult.IsFailure) return false;
        var address = addressCreateResult.Value;

        var basketAddAddressResult = basket.AddAddress(address);
        if (basketAddAddressResult.IsFailure) return false;

        //Сохраняем агрегат
        _basketRepository.Update(basket);
        return await _unitOfWork.SaveChangesAsync(cancellationToken);
    }
}