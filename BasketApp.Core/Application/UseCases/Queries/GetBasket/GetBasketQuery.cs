﻿using MediatR;

namespace BasketApp.Core.Application.UseCases.Queries.GetBasket;

/// <summary>
///     Получить состав корзины
/// </summary>
public class GetBasketQuery : IRequest<GetBasketQueryModel>
{
    public GetBasketQuery(Guid basketId)
    {
        if (basketId == Guid.Empty) throw new ArgumentException(nameof(basketId));
        BasketId = basketId;
    }

    public Guid BasketId { get; }
}