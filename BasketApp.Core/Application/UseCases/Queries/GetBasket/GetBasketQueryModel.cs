﻿namespace BasketApp.Core.Application.UseCases.Queries.GetBasket;

public class GetBasketQueryModel
{
    public GetBasketQueryModel(BasketQueryModel basket)
    {
        Basket = basket;
    }

    public BasketQueryModel Basket { get; set; }
}

public class BasketQueryModel
{
    private BasketQueryModel()
    {
    }

    public BasketQueryModel(Guid id, AddressQueryModel address, List<ItemQueryModel> items, int deliveryPeriod,
        string status) : this()
    {
        Id = id;
        Address = address;
        Items = items;
        DeliveryPeriod = deliveryPeriod;
        Status = status;
    }

    public Guid Id { get; set; }

    public AddressQueryModel Address { get; set; }

    public List<ItemQueryModel> Items { get; set; }

    public int DeliveryPeriod { get; set; }

    public string Status { get; set; }
}

public class AddressQueryModel
{
    private AddressQueryModel()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    public AddressQueryModel(string country, string city, string street, string house, string apartment) : this()
    {
        Country = country;
        City = city;
        Street = street;
        House = house;
        Apartment = apartment;
    }

    /// <summary>
    ///     Страна
    /// </summary>
    public string Country { get; }

    /// <summary>
    ///     Город
    /// </summary>
    public string City { get; }

    /// <summary>
    ///     Улица
    /// </summary>
    public string Street { get; }

    /// <summary>
    ///     Дом
    /// </summary>
    public string House { get; }

    /// <summary>
    ///     Квартира
    /// </summary>
    public string Apartment { get; }
}

public class ItemQueryModel
{
    /// <summary>
    ///     Ctr
    /// </summary>
    private ItemQueryModel()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    public ItemQueryModel(Guid id, Guid goodId, int quantity) : this()
    {
        Id = id;
        GoodId = goodId;
        Quantity = quantity;
    }

    /// <summary>
    ///     Идентификатор
    /// </summary>
    public Guid Id { get; }

    /// <summary>
    ///     Товар
    /// </summary>
    public Guid GoodId { get; }

    /// <summary>
    ///     Количество
    /// </summary>
    public int Quantity { get; }
}