﻿using Dapper;
using MediatR;
using Npgsql;

namespace BasketApp.Core.Application.UseCases.Queries.GetBasket;

public class GetBasketHandler : IRequestHandler<GetBasketQuery, GetBasketQueryModel>
{
    private readonly string _connectionString;

    public GetBasketHandler(string connectionString)
    {
        _connectionString = !string.IsNullOrWhiteSpace(connectionString)
            ? connectionString
            : throw new ArgumentNullException(nameof(connectionString));
    }

    public async Task<GetBasketQueryModel> Handle(GetBasketQuery message, CancellationToken cancellationToken)
    {
        using var connection = new NpgsqlConnection(_connectionString);
        connection.Open();

        var result = await connection.QueryAsync<dynamic>(
            @"SELECT i.id as item_id,i.good_id,i.quantity,b.id as basket_id, b.address_country, b.address_city,
                b.address_street, b.address_house, b.address_apartment, b.delivery_period_id, b.status, b.total
                FROM public.items as i
                LEFT JOIN public.baskets as b on i.basket_id = b.id
                where b.id=@id;"
            , new { id = message.BasketId });

        if (result.AsList().Count == 0)
            return null;

        return new GetBasketQueryModel(MapBasket(result));
    }

    private BasketQueryModel MapBasket(dynamic result)
    {
        var address = new AddressQueryModel(
            result[0].address_country,
            result[0].address_city,
            result[0].address_street,
            result[0].address_house,
            result[0].address_apartment);

        var items = new List<ItemQueryModel>();
        foreach (var dItem in result)
        {
            var item = new ItemQueryModel(dItem.item_id, dItem.good_id, dItem.quantity);
            items.Add(item);
        }

        var deliveryPeriod = result[0].delivery_period_id;
        var status = result[0].status;

        var basket = new BasketQueryModel(result[0].basket_id, address, items, deliveryPeriod, status);
        return basket;
    }
}