﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Domain.Model.SharedKernel;
using CSharpFunctionalExtensions;
using Primitives;

namespace BasketApp.Core.Ports;

public interface IDiscountClient
{
    /// <summary>
    ///     Получить информацию о скидке
    /// </summary>
    Task<Result<Discount, Error>> GetDiscountAsync(Basket basket, CancellationToken cancellationToken);
}