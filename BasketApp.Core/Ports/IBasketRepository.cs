﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using Primitives;

namespace BasketApp.Core.Ports;

/// <summary>
///     Repository для Aggregate Basket
/// </summary>
public interface IBasketRepository : IRepository<Basket>
{
    /// <summary>
    ///     Добавить новую корзину
    /// </summary>
    /// <param name="basket">Корзина</param>
    /// <returns>Корзина</returns>
    Task AddAsync(Basket basket);

    /// <summary>
    ///     Обновить существующую корзину
    /// </summary>
    /// <param name="basket">Корзина</param>
    void Update(Basket basket);

    /// <summary>
    ///     Получить корзину по идентификатору
    /// </summary>
    /// <param name="basketId">Идентификатор</param>
    /// <returns>Корзина</returns>
    Task<Basket> GetAsync(Guid basketId);
    
    /// <summary>
    ///     Получить корзину по идентификатору покупателя
    /// </summary>
    /// <param name="basketId">Идентификатор покупателя</param>
    /// <returns>Корзина</returns>
    Task<Basket> GetByBuyerIdAsync(Guid basketId);
    
    
}