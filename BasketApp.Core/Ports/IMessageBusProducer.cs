﻿using BasketApp.Core.Domain.Model.BasketAggregate.DomainEvents;

namespace BasketApp.Core.Ports;

public interface IMessageBusProducer
{
    Task Publish(BasketConfirmedDomainEvent notification, CancellationToken cancellationToken);
}