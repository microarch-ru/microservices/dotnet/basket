﻿# OpenApi
Вызывать из папки BasketApp.Api/Adapters/Http/Contract
```
cd BasketApp.Api/Adapters/Http/Contract/
openapi-generator generate -i https://gitlab.com/microarch-ru/microservices/dotnet/system-design/-/raw/main/services/basket/contracts/openapi.yml -g aspnetcore -o . --package-name OpenApi --additional-properties classModifier=abstract --additional-properties operationResultTask=true
```
# БД 
```
dotnet tool install --global dotnet-ef
dotnet tool update --global dotnet-ef
dotnet add package Microsoft.EntityFrameworkCore.Design
```
[Подробнее про dotnet cli](https://learn.microsoft.com/ru-ru/ef/core/cli/dotnet)

# Миграции
```
dotnet ef migrations add Init --startup-project ./BasketApp.Api --project ./BasketApp.Infrastructure --output-dir ./Adapters/Postgres/Migrations
dotnet ef database update --startup-project ./BasketApp.Api --connection "Server=localhost;Port=5432;User Id=username;Password=secret;Database=basket;"
```

# Запросы к БД
```
SELECT * FROM public.statuses;
SELECT * FROM public.delivery_periods;
SELECT * FROM public.goods;

SELECT b.*,s.name,t.name as delivery_period FROM public.baskets as b 
LEFT JOIN public.statuses as s on b.status_id=s.id 
LEFT JOIN public.delivery_periods as t on b.delivery_period_id=t.id;

SELECT * FROM public.items;
SELECT * FROM public.outbox;
```

# Очистка БД (все кроме справочников)
```
DELETE FROM public.baskets;
DELETE FROM public.items;
DELETE FROM public.outbox;
```