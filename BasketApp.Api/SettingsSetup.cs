using BasketApp.Infrastructure;
using Microsoft.Extensions.Options;

namespace BasketApp.Api;

public class SettingsSetup : IConfigureOptions<Settings>
{
    private readonly IConfiguration _configuration;

    public SettingsSetup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void Configure(Settings options)
    {
        options.ConnectionString = _configuration["CONNECTION_STRING"];
        options.DiscountServiceGrpcHost = _configuration["DISCOUNT_SERVICE_GRPC_HOST"];
        options.MessageBrokerHost = _configuration["MESSAGE_BROKER_HOST"];
        options.StocksChangedTopic = _configuration["STOCKS_CHANGED_TOPIC"];
        options.BasketConfirmedTopic = _configuration["BASKET_CONFIRMED_TOPIC"];
    }
}