using System.Reflection;
using BasketApp.Api;
using BasketApp.Api.Adapters.Http.Contract.src.OpenApi.Filters;
using BasketApp.Api.Adapters.Http.Contract.src.OpenApi.Formatters;
using BasketApp.Api.Adapters.Http.Contract.src.OpenApi.OpenApi;
using BasketApp.Api.Adapters.Kafka.StocksChanged;
using BasketApp.Core.Application.DomainEventHandlers;
using BasketApp.Core.Application.UseCases.Commands.AddAddress;
using BasketApp.Core.Application.UseCases.Commands.AddDeliveryPeriod;
using BasketApp.Core.Application.UseCases.Commands.ChangeItems;
using BasketApp.Core.Application.UseCases.Commands.ChangeStocks;
using BasketApp.Core.Application.UseCases.Commands.Checkout;
using BasketApp.Core.Application.UseCases.Commands.TestCheckout;
using BasketApp.Core.Application.UseCases.Queries.GetBasket;
using BasketApp.Core.Domain.Model.BasketAggregate.DomainEvents;
using BasketApp.Core.Domain.Services.PromoGoodService;
using BasketApp.Core.Ports;
using BasketApp.Infrastructure.Adapters.Grpc.DiscountService;
using BasketApp.Infrastructure.Adapters.Kafka.BasketConfirmed;
using BasketApp.Infrastructure.Adapters.Postgres;
using BasketApp.Infrastructure.Adapters.Postgres.BackgroundJobs;
using BasketApp.Infrastructure.Adapters.Postgres.Repositories;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Primitives;
using Quartz;

var builder = WebApplication.CreateBuilder(args);

// Health Checks
builder.Services.AddHealthChecks();

// Cors
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.AllowAnyOrigin(); // Не делайте так в проде!
        });
});

// Configuration
builder.Services.ConfigureOptions<SettingsSetup>();
var connectionString = builder.Configuration["CONNECTION_STRING"];

// Domain Services
builder.Services.AddTransient<IPromoGoodService, PromoGoodService>();

// БД, ORM 
builder.Services.AddDbContext<ApplicationDbContext>((_, optionsBuilder) =>
    {
        optionsBuilder.UseNpgsql(connectionString,
            sqlOptions => { sqlOptions.MigrationsAssembly("BasketApp.Infrastructure"); });
        optionsBuilder.EnableSensitiveDataLogging();
    }
);

// UnitOfWork
builder.Services.AddScoped<IUnitOfWork, UnitOfWorkV3>();

// Repositories
builder.Services.AddScoped<IGoodRepository, GoodRepository>();
builder.Services.AddScoped<IBasketRepository, BasketRepository>();

// Mediator
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

// Commands
builder.Services.AddTransient<IRequestHandler<ChangeItemsCommand, bool>, ChangeItemsHandler>();
builder.Services.AddTransient<IRequestHandler<AddAddressCommand, bool>, AddAddressHandler>();
builder.Services.AddTransient<IRequestHandler<AddDeliveryPeriodCommand, bool>, AddDeliveryPeriodHandler>();
builder.Services.AddTransient<IRequestHandler<CheckoutCommand, UnitResult<Error>>, CheckoutHandler>();
builder.Services.AddTransient<IRequestHandler<ChangeStocksCommand, bool>, ChangeStocksHandler>();
builder.Services.AddTransient<IRequestHandler<TestCheckoutCommand, bool>, TestCheckoutHandler>();

// Queries
builder.Services.AddTransient<IRequestHandler<GetBasketQuery, GetBasketQueryModel>>(_ =>
    new GetBasketHandler(connectionString));

// HTTP Handlers
builder.Services.AddControllers(options => { options.InputFormatters.Insert(0, new InputFormatterStream()); })
    .AddNewtonsoftJson(options =>
    {
        options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        options.SerializerSettings.Converters.Add(new StringEnumConverter
        {
            NamingStrategy = new CamelCaseNamingStrategy()
        });
    });

// Swagger
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("1.0.0", new OpenApiInfo
    {
        Title = "Basket Service",
        Description = "Отвечает за формирование корзины и оформление заказа",
        Contact = new OpenApiContact
        {
            Name = "Kirill Vetchinkin",
            Url = new Uri("https://microarch.ru"),
            Email = "info@microarch.ru"
        }
    });
    options.CustomSchemaIds(type => type.FriendlyId(true));
    options.IncludeXmlComments(
        $"{AppContext.BaseDirectory}{Path.DirectorySeparatorChar}{Assembly.GetEntryAssembly()?.GetName().Name}.xml");
    options.DocumentFilter<BasePathFilter>("");
    options.OperationFilter<GeneratePathParamsValidationFilter>();
});
builder.Services.AddSwaggerGenNewtonsoftSupport();

// gRPC
builder.Services.AddTransient<IDiscountClient, Client>();

// Message Broker Consumer
builder.Services.Configure<HostOptions>(options =>
{
    options.BackgroundServiceExceptionBehavior = BackgroundServiceExceptionBehavior.Ignore;
    options.ShutdownTimeout = TimeSpan.FromSeconds(30);
});
builder.Services.AddHostedService<ConsumerService>();

// Domain Event Handlers
builder.Services.AddTransient<INotificationHandler<BasketConfirmedDomainEvent>, BasketConfirmedDomainEventHandler>();

// Message Broker Producer
builder.Services.AddTransient<IMessageBusProducer, Producer>();

// CRON Jobs
builder.Services.AddQuartz(configure =>
{
    var processOutboxMessagesJobKey = new JobKey(nameof(ProcessOutboxMessagesJob));
    configure
        .AddJob<ProcessOutboxMessagesJob>(processOutboxMessagesJobKey)
        .AddTrigger(
            trigger => trigger.ForJob(processOutboxMessagesJobKey)
                .WithSimpleSchedule(
                    schedule => schedule.WithIntervalInSeconds(3)
                        .RepeatForever()));
    configure.UseMicrosoftDependencyInjectionJobFactory();
});
builder.Services.AddQuartzHostedService();

var app = builder.Build();

// -----------------------------------
// Configure the HTTP request pipeline
if (app.Environment.IsDevelopment())
    app.UseDeveloperExceptionPage();
else
    app.UseHsts();

app.UseHealthChecks("/health");
app.UseRouting();

app.UseDefaultFiles();
app.UseStaticFiles();
app.UseSwagger(c => { c.RouteTemplate = "openapi/{documentName}/openapi.json"; })
    .UseSwaggerUI(options =>
    {
        options.RoutePrefix = "openapi";
        options.SwaggerEndpoint("/openapi/1.0.0/openapi.json", "Swagger Basket Service");
        options.RoutePrefix = string.Empty;
        options.SwaggerEndpoint("/openapi-original.json", "Swagger Basket Service");
    });

app.UseCors();
app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

// Apply Migrations
using (var scope = app.Services.CreateScope())
{
    var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    db.Database.Migrate();
}

app.Run();