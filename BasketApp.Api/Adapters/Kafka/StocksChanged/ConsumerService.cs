using BasketApp.Core.Application.UseCases.Commands.ChangeStocks;
using BasketApp.Infrastructure;
using Confluent.Kafka;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StocksChanged;

namespace BasketApp.Api.Adapters.Kafka.StocksChanged;

public class ConsumerService : BackgroundService
{
    private readonly IMediator _mediator;
    private readonly IConsumer<Ignore, string> _consumer;
    private readonly string _topic;

    public ConsumerService(IMediator mediator, IOptions<Settings> settings)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        if (string.IsNullOrWhiteSpace(settings.Value.MessageBrokerHost)) throw new ArgumentException(nameof(settings.Value.MessageBrokerHost));
        if (string.IsNullOrWhiteSpace(settings.Value.StocksChangedTopic)) throw new ArgumentException(nameof(settings.Value.StocksChangedTopic));

        var consumerConfig = new ConsumerConfig
        {
            BootstrapServers = settings.Value.MessageBrokerHost,
            GroupId = "BasketConsumerGroup",
            EnableAutoOffsetStore = false,
            EnableAutoCommit = true,
            AutoOffsetReset = AutoOffsetReset.Earliest,
            EnablePartitionEof = true
        };
        _consumer = new ConsumerBuilder<Ignore, string>(consumerConfig).Build();
        _topic = settings.Value.StocksChangedTopic;
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        _consumer.Subscribe(_topic);
        try
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(100), cancellationToken);
                var consumeResult = _consumer.Consume(cancellationToken);

                if (consumeResult.IsPartitionEOF) continue;

                var stocksChangedIntegrationEvent =
                    JsonConvert.DeserializeObject<StocksChangedIntegrationEvent>(consumeResult.Message.Value);

                var changeStocksCommand = new ChangeStocksCommand(
                    Guid.Parse(stocksChangedIntegrationEvent.GoodId),
                    stocksChangedIntegrationEvent.Quantity);

                var response = await _mediator.Send(changeStocksCommand, cancellationToken);
                if (!response) Console.WriteLine("Error");

                try
                {
                    _consumer.StoreOffset(consumeResult);
                }
                catch (KafkaException e)
                {
                    Console.WriteLine($"Store Offset error: {e.Error.Reason}");
                }
            }
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine(e.Message);
        }
    }
}