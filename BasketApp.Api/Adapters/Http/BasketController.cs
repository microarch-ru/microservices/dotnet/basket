﻿using BasketApp.Api.Adapters.Http.Contract.src.OpenApi.Controllers;
using BasketApp.Api.Adapters.Http.Contract.src.OpenApi.Models;
using BasketApp.Core.Application.UseCases.Commands.AddAddress;
using BasketApp.Core.Application.UseCases.Commands.AddDeliveryPeriod;
using BasketApp.Core.Application.UseCases.Commands.ChangeItems;
using BasketApp.Core.Application.UseCases.Commands.Checkout;
using BasketApp.Core.Application.UseCases.Commands.TestCheckout;
using BasketApp.Core.Application.UseCases.Queries.GetBasket;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Primitives.Extensions;

namespace BasketApp.Api.Adapters.Http;

public class BasketController : DefaultApiController
{
    private readonly IMediator _mediator;

    public BasketController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    public override async Task<IActionResult> AddAddress(Guid basketId, Address address)
    {
        var addAddressCommand = new AddAddressCommand(basketId, address.Country,
            address.City, address.Street, address.House, address.Apartment);

        var response = await _mediator.Send(addAddressCommand);
        if (response) return Ok();
        return Conflict();
    }

    public override async Task<IActionResult> AddDeliveryPeriod(Guid basketId, string body)
    {
        var addDeliveryPeriodCommand =
            new AddDeliveryPeriodCommand(basketId, body);

        var response = await _mediator.Send(addDeliveryPeriodCommand);
        if (response) return Ok();
        return Conflict();
    }

    public override async Task<IActionResult> ChangeItems(Guid basketId, Item item)
    {
        var changeItemsCommand =
            new ChangeItemsCommand(basketId, item.GoodId, item.Quantity);

        var response = await _mediator.Send(changeItemsCommand);
        if (response) return Ok();
        return Conflict();
    }

    public override async Task<IActionResult> Checkout(Guid basketId)
    {
        var checkoutCommand = new CheckoutCommand(basketId);

        var response = await _mediator.Send(checkoutCommand);
        if (response.IsSuccess) return Ok();
        return Conflict(response.Error);
    }

    public override async Task<IActionResult> GetBasket(Guid basketId)
    {
        var getBasketQuery = new GetBasketQuery(basketId);

        var response = await _mediator.Send(getBasketQuery);

        // Мапим результат Query на Model
        if (response == null) return NotFound();
        var model = new Basket
        {
            Id = response.Basket.Id,
            Address = ToAddressModelMapper(response.Basket.Address),
            DeliveryPeriod = (DeliveryPeriod)response.Basket.DeliveryPeriod,
            Items = ToItemModelMapper(response.Basket.Items),
            Status = ToStatusModelMapper(response.Basket.Status) 
        };
        return Ok(model);
    }

    /// <summary>
    ///     Этот метод исключительно для тестирования!
    /// </summary>
    public override async Task<IActionResult> TestCheckout()
    {
        var testCheckoutCommand = new TestCheckoutCommand();

        var response = await _mediator.Send(testCheckoutCommand);
        if (response) return Ok();
        return Conflict();
    }

    private Address ToAddressModelMapper(AddressQueryModel address)
    {
        var addressModel = new Address
        {
            Country = address.Country,
            City = address.City,
            Street = address.Street,
            House = address.House,
            Apartment = address.Apartment
        };
        return addressModel;
    }

    private List<ExistedItem> ToItemModelMapper(List<ItemQueryModel> items)
    {
        var existedItems = items.Select(i => new ExistedItem
        {
            Id = i.Id,
            GoodId = i.GoodId,
            Quantity = i.Quantity
        });
        return existedItems.ToList();
    }
    
    private Basket.StatusEnum ToStatusModelMapper(string status)
    {
        return (status + "Enum").ToEnum<Basket.StatusEnum>();
    }
}