using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Domain.Model.GoodAggregate;
using BasketApp.Core.Domain.Model.SharedKernel;
using BasketApp.Infrastructure.Adapters.Postgres;
using BasketApp.Infrastructure.Adapters.Postgres.Repositories;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;

namespace BasketApp.IntegrationTests.RepositoriesV2;

public class BasketRepositoryShould : IAsyncLifetime
{
    private readonly Address _address;
    private readonly IMediator _mediator;

    /// <summary>
    ///     Настройка Postgres из библиотеки TestContainers
    /// </summary>
    /// <remarks>По сути это Docker контейнер с Postgres</remarks>
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("basket")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();

    private ApplicationDbContext _context;

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <remarks>Вызывается один раз перед всеми тестами в рамках этого класса</remarks>
    public BasketRepositoryShould()
    {
        _mediator = Substitute.For<IMediator>();
        var addressCreateResut = Address.Create("Россия", "Москва", "Тверская", "1", "2");
        addressCreateResut.IsSuccess.Should().BeTrue();
        _address = addressCreateResut.Value;
    }

    /// <summary>
    ///     Инициализируем окружение
    /// </summary>
    /// <remarks>Вызывается перед каждым тестом</remarks>
    public async Task InitializeAsync()
    {
        //Стартуем БД (библиотека TestContainers запускает Docker контейнер с Postgres)
        await _postgreSqlContainer.StartAsync();

        //Накатываем миграции и справочники
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
            _postgreSqlContainer.GetConnectionString(),
            sqlOptions => { sqlOptions.MigrationsAssembly("BasketApp.Infrastructure"); }).Options;
        _context = new ApplicationDbContext(contextOptions);
        _context.Database.Migrate();
    }

    /// <summary>
    ///     Уничтожаем окружение
    /// </summary>
    /// <remarks>Вызывается после каждого теста</remarks>
    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async Task CanAddBasket()
    {
        //Arrange
        var buyerId = Guid.NewGuid();
        var basket = Basket.Create(buyerId).Value;
        basket.AddAddress(_address);
        basket.AddDeliveryPeriod(DeliveryPeriod.Morning);
        basket.Change(Good.Coffee, 1);
        basket.Change(Good.Milk, 2);
        basket.Change(Good.Sugar, 3);

        //Act
        var basketRepository = new BasketRepository(_context);
        await basketRepository.AddAsync(basket);
        var unitOfWork = new UnitOfWorkV2(_context, _mediator);
        await unitOfWork.SaveChangesAsync();

        //Assert
        var basketFromDb = await basketRepository.GetAsync(basket.Id);
        basket.Should().BeEquivalentTo(basketFromDb);
    }

    [Fact]
    public async Task CanUpdateBasket()
    {
        //Arrange
        var buyerId = Guid.NewGuid();
        var basket = Basket.Create(buyerId).Value;
        basket.AddAddress(_address);
        basket.AddDeliveryPeriod(DeliveryPeriod.Morning);
        basket.Change(Good.Coffee, 1);
        basket.Change(Good.Milk, 2);
        basket.Change(Good.Sugar, 3);

        var basketRepository = new BasketRepository(_context);
        await basketRepository.AddAsync(basket);
        var unitOfWork = new UnitOfWorkV2(_context, _mediator);
        await unitOfWork.SaveChangesAsync();

        //Act
        basket.Change(Good.Coffee, 3);
        basketRepository.Update(basket);

        //Assert
        var basketFromDb = await basketRepository.GetAsync(basket.Id);
        basket.Should().BeEquivalentTo(basketFromDb);
        basket.Items.SingleOrDefault(c => c.Id == Good.Coffee.Id)?.Quantity.Should().Be(3);
    }

    [Fact]
    public async Task CanGetById()
    {
        //Arrange
        var buyerId = Guid.NewGuid();
        var basket = Basket.Create(buyerId).Value;
        basket.AddAddress(_address);
        basket.AddDeliveryPeriod(DeliveryPeriod.Morning);
        basket.Change(Good.Coffee, 1);
        basket.Change(Good.Milk, 2);
        basket.Change(Good.Sugar, 3);

        //Act
        var basketRepository = new BasketRepository(_context);
        await basketRepository.AddAsync(basket);
        var unitOfWork = new UnitOfWorkV2(_context,_mediator);
        await unitOfWork.SaveChangesAsync();

        //Assert
        var basketFromDb = await basketRepository.GetAsync(basket.Id);
        basket.Should().BeEquivalentTo(basketFromDb);
    }

    [Fact]
    public async Task CanGetByBuyerId()
    {
        //Arrange
        var buyerId = Guid.NewGuid();
        var basket = Basket.Create(buyerId).Value;
        basket.AddAddress(_address);
        basket.AddDeliveryPeriod(DeliveryPeriod.Morning);
        basket.Change(Good.Coffee, 1);
        basket.Change(Good.Milk, 2);
        basket.Change(Good.Sugar, 3);

        //Act
        var basketRepository = new BasketRepository(_context);
        await basketRepository.AddAsync(basket);
        var unitOfWork = new UnitOfWorkV2(_context,_mediator);
        await unitOfWork.SaveChangesAsync();

        //Assert
        var basketFromDb = await basketRepository.GetByBuyerIdAsync(buyerId);
        basket.Should().BeEquivalentTo(basketFromDb);
    }
}