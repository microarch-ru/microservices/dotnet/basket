using System;
using System.Threading;
using System.Threading.Tasks;
using BasketApp.Core.Application.UseCases.Commands.Checkout;
using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Domain.Model.GoodAggregate;
using BasketApp.Core.Domain.Model.SharedKernel;
using BasketApp.Core.Domain.Services.PromoGoodService;
using BasketApp.Core.Ports;
using CSharpFunctionalExtensions;
using FluentAssertions;
using NSubstitute;
using Primitives;
using Xunit;

namespace BasketApp.UnitTests.Application;

public class CheckoutCommandShould
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IBasketRepository _basketRepositoryMock;
    private readonly IPromoGoodService _promoGoodServiceMock;
    private readonly IDiscountClient _discountClientMock;
    
    public CheckoutCommandShould()
    {
        _unitOfWork = Substitute.For<IUnitOfWork>();
        _basketRepositoryMock = Substitute.For<IBasketRepository>();
        _promoGoodServiceMock = Substitute.For<IPromoGoodService>();
        _discountClientMock = Substitute.For<IDiscountClient>();
    }

    [Fact]
    public async Task ReturnTrueWhenBasketIsCorrect()
    {
        //Arrange
        var basketId = Guid.NewGuid();
        _unitOfWork.SaveChangesAsync().Returns(Task.FromResult(true));
        _basketRepositoryMock.GetAsync(Arg.Any<Guid>()).Returns(Task.FromResult(ReadyToCheckoutBasket(basketId)));
        _promoGoodServiceMock.AddPromo(Arg.Any<Basket>()).Returns(UnitResult.Success<Error>());
        _discountClientMock.GetDiscountAsync(Arg.Any<Basket>(), Arg.Any<CancellationToken>())
            .Returns(Task.FromResult(Discount10Percent()));

        var command = new CheckoutCommand(basketId);
        var handler = new CheckoutHandler(_unitOfWork, _basketRepositoryMock, _promoGoodServiceMock, _discountClientMock);

        //Act
        var result = await handler.Handle(command, new CancellationToken());

        //Assert
        result.IsSuccess.Should().BeTrue();
    }

    private Good CorrectGood()
    {
        return Good.Bread;
    }

    private Basket CorrectBasket(Guid basketId)
    {
        return Basket.Create(basketId).Value;
    }

    private Address CorrectAddress()
    {
        var address = Address.Create("Россия", "Москва", "Тверская", "1", "2").Value;
        return address;
    }

    private Result<Discount, Error> Discount10Percent()
    {
        var discount = Discount.Create(0.1);
        return discount;
    }

    private Basket ReadyToCheckoutBasket(Guid basketId)
    {
        var basket = CorrectBasket(basketId);
        basket.Change(CorrectGood(), 1);
        basket.AddAddress(CorrectAddress());
        basket.AddDeliveryPeriod(DeliveryPeriod.Morning);
        return basket;
    }
}