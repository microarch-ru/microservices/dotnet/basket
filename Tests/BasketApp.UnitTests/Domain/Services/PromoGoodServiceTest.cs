using System;
using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Domain.Model.GoodAggregate;
using BasketApp.Core.Domain.Services.PromoGoodService;
using FluentAssertions;
using Xunit;

namespace BasketApp.UnitTests.Domain.Services;

public class PromoGoodServiceShould
{
    [Fact]
    public void AddGumToBasketWhenTotalIsMoreThen1000()
    {
        // Arrange
        var buyerId = Guid.NewGuid();
        var basket = Basket.Create(buyerId).Value;
        basket.Change(Good.Bread, 11); // 100 * 11 = 1100 рублей
        
        // Act
        var promoGoodService = new PromoGoodService();
        var result = promoGoodService.AddPromo(basket); // Gum за 1 рубль

        // Assert
        result.IsSuccess.Should().BeTrue();
        basket.Items.Should().Contain(c=>c.GoodId == Good.Gum.Id);
        basket.Total.Should().Be(1101);
    }
}