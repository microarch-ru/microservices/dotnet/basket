using BasketApp.Core.Domain.Model.SharedKernel;
using FluentAssertions;
using Xunit;

namespace BasketApp.UnitTests.Domain.Model.SharedKernel;

public class DiscountShould
{
    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    public void BeCorrectWhenParamsAreCorrectOnCreated(double percents)
    {
        //Arrange

        //Act
        var weight = Discount.Create(percents);

        //Assert
        weight.IsSuccess.Should().BeTrue();
        weight.Value.Value.Should().Be(percents);
    }

    [Theory]
    [InlineData(-0.1)]
    [InlineData(1.1)]
    public void ReturnErrorWhenParamsAreNotCorrectOnCreated(double percents)
    {
        //Arrange

        //Act
        var weight = Discount.Create(percents);

        //Assert
        weight.IsSuccess.Should().BeFalse();
        weight.Error.Should().NotBeNull();
    }

    [Fact]
    public void BeEqualWhenAllPropertiesIsEqual()
    {
        //Arrange
        var first = Discount.Create(1).Value;
        var second = Discount.Create(1).Value;

        //Act
        var result = first == second;

        //Assert
        result.Should().BeTrue();
    }

    [Fact]
    public void BeNotEqualWhenAllPropertiesIsEqual()
    {
        //Arrange
        var first = Discount.Create(0).Value;
        var second = Discount.Create(1).Value;

        //Act
        var result = first == second;

        //Assert
        result.Should().BeFalse();
    }

    [Fact]
    public void CanCompareMoreThen()
    {
        //Arrange
        var first = Discount.Create(1).Value;
        var second = Discount.Create(0).Value;

        //Act
        var result = first > second;

        //Assert
        result.Should().BeTrue();
    }

    [Fact]
    public void CanCompareLessThen()
    {
        //Arrange
        var first = Discount.Create(0).Value;
        var second = Discount.Create(1).Value;

        //Act
        var result = first < second;

        //Assert
        result.Should().BeTrue();
    }
}