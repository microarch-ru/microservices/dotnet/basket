using BasketApp.Core.Domain.Model.GoodAggregate;
using FluentAssertions;
using Xunit;

namespace BasketApp.UnitTests.Domain.Model.GoodAggregate;

public class GoodTest
{
    [Fact]
    public void BeCorrectWhenParamsAreCorrectOnCreated()
    {
        //Arrange

        //Act
        var bread = Good.Bread;

        //Assert
        bread.Title.Should().Be("Хлеб");
        bread.Description.Should().Be("Описание хлеба");
        bread.Price.Should().Be(100);
        bread.Weight.Value.Should().Be(6);
    }
}