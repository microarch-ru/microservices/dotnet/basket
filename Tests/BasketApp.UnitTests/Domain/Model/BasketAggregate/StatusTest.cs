using BasketApp.Core.Domain.Model.BasketAggregate;
using FluentAssertions;
using Xunit;

namespace BasketApp.UnitTests.Domain.Model.BasketAggregate;

public class StatusShould
{
    [Fact]
    public void ReturnCorrectName()
    {
        //Arrange
        
        //Act

        //Assert
        Status.Created.Name.Should().Be("created");
        Status.Confirmed.Name.Should().Be("confirmed");
    }
    
    [Fact]
    public void BeEqualWhenAllPropertiesIsEqual()
    {
        //Arrange
        
        //Act
        // ReSharper disable once EqualExpressionComparison
        var result = Status.Created == Status.Created;

        //Assert
        result.Should().BeTrue();
    }

    [Fact]
    public void BeNotEqualWhenAllPropertiesIsEqual()
    {
        //Arrange
       
        //Act
        var result = Status.Created == Status.Confirmed;

        //Assert
        result.Should().BeFalse();
    }
}