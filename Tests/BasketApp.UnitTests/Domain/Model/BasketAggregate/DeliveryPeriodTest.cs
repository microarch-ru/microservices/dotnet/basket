using BasketApp.Core.Domain.Model.BasketAggregate;
using FluentAssertions;
using Xunit;

namespace BasketApp.UnitTests.Domain.Model.BasketAggregate;

public class DeliveryPeriodShould
{
    [Fact]
    public void ReturnCorrectIdAndName()
    {
        //Arrange

        //Act
        var morning = DeliveryPeriod.Morning;
        var midday = DeliveryPeriod.Midday;
        var evening = DeliveryPeriod.Evening;
        var night = DeliveryPeriod.Night;

        //Assert
        morning.Id.Should().Be(1);
        morning.Name.Should().Be("morning");
        morning.From.Should().Be(6);
        morning.To.Should().Be(12);

        midday.Id.Should().Be(2);
        midday.Name.Should().Be("midday");
        midday.From.Should().Be(12);
        midday.To.Should().Be(17);

        evening.Id.Should().Be(3);
        evening.Name.Should().Be("evening");
        evening.From.Should().Be(17);
        evening.To.Should().Be(24);

        night.Id.Should().Be(4);
        night.Name.Should().Be("night");
        night.From.Should().Be(0);
        night.To.Should().Be(6);
    }

    [Theory]
    [InlineData(1, "morning")]
    [InlineData(2, "midday")]
    [InlineData(3, "evening")]
    [InlineData(4, "night")]
    public void CanBeFoundById(int id, string name)
    {
        //Arrange

        //Act
        var deliveryPeriod = DeliveryPeriod.FromId(id).Value;

        //Assert
        deliveryPeriod.Id.Should().Be(id);
        deliveryPeriod.Name.Should().Be(name);
    }

    [Fact]
    public void ReturnErrorWhenStatusNotFoundById()
    {
        //Arrange
        var id = -1;

        //Act
        var result = DeliveryPeriod.FromId(id);

        //Assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().NotBeNull();
    }

    [Theory]
    [InlineData(1, "morning")]
    [InlineData(2, "midday")]
    [InlineData(3, "evening")]
    [InlineData(4, "night")]
    public void CanBeFoundByName(int id, string name)
    {
        //Arrange

        //Act
        var deliveryPeriod = DeliveryPeriod.FromName(name).Value;

        //Assert
        deliveryPeriod.Id.Should().Be(id);
        deliveryPeriod.Name.Should().Be(name);
    }

    [Fact]
    public void ReturnErrorWhenStatusNotFoundByName()
    {
        //Arrange
        var name = "not-existed-status";

        //Act
        var result = DeliveryPeriod.FromName(name);

        //Assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnListOfDeliveryPeriods()
    {
        //Arrange

        //Act
        var allDeliveryPeriods = DeliveryPeriod.List();

        //Assert
        allDeliveryPeriods.Should().NotBeEmpty();
    }
}