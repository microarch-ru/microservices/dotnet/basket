using System;
using System.Threading.Tasks;
using BasketApp.Api.Adapters.Http;
using BasketApp.Core.Application.UseCases.Commands.Checkout;
using BasketApp.Core.Domain.Model.BasketAggregate;
using CSharpFunctionalExtensions;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using Primitives;
using Xunit;

namespace BasketApp.UnitTests.Ui.Http;

public class BasketControllerShould
{
    private readonly Guid _basketId = Guid.NewGuid();
    private readonly IMediator _mediator = Substitute.For<IMediator>();

    [Fact]
    public async Task CheckoutBasketCorrectly()
    {
        // Arrange
        _mediator.Send(Arg.Any<CheckoutCommand>())
            .Returns(UnitResult.Success<Error>());

        // Act
        var basketController = new BasketController(_mediator);
        var result = await basketController.Checkout(_basketId);

        // Assert
        result.Should().BeOfType<OkResult>();
    }
    
    [Fact]
    public async Task ReturnConflictWhenCheckoutBasketNotCorrectly()
    {
        // Arrange
        _mediator.Send(Arg.Any<CheckoutCommand>())
            .Returns(UnitResult.Failure(Basket.Errors.BasketHasAlreadyBeenIssued()));

        // Act
        var basketController = new BasketController(_mediator);
        var result = await basketController.Checkout(_basketId);

        // Assert
        result.Should().BeOfType<ConflictObjectResult>();
    }
}