﻿namespace BasketApp.Infrastructure;

public class Settings
{
    public string ConnectionString { get; set; }
    public string DiscountServiceGrpcHost { get; set; }
    public string MessageBrokerHost { get; set; }
    public string StocksChangedTopic { get; set; }
    public string BasketConfirmedTopic { get; set; }
}