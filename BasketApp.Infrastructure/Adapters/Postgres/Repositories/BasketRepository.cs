﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Ports;
using Microsoft.EntityFrameworkCore;

namespace BasketApp.Infrastructure.Adapters.Postgres.Repositories;

public class BasketRepository : IBasketRepository
{
    private readonly ApplicationDbContext _dbContext;

    public BasketRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public async Task AddAsync(Basket basket)
    {
        if (basket.DeliveryPeriod != null) _dbContext.Attach(basket.DeliveryPeriod);
        await _dbContext.Baskets.AddAsync(basket);
    }

    public void Update(Basket basket)
    {
        if (basket.DeliveryPeriod != null) _dbContext.Attach(basket.DeliveryPeriod);
        _dbContext.Baskets.Update(basket);
    }

    public async Task<Basket> GetAsync(Guid basketId)
    {
        var basket = await _dbContext
            .Baskets
            .Include(x => x.DeliveryPeriod)
            .FirstOrDefaultAsync(o => o.Id == basketId);
        if (basket != null)
            await _dbContext.Entry(basket)
                .Collection(i => i.Items).LoadAsync();

        return basket;
    }
    
    public async Task<Basket> GetByBuyerIdAsync(Guid buyerId)
    {
        var basket = await _dbContext
            .Baskets
            .Include(x => x.DeliveryPeriod)
            .FirstOrDefaultAsync(o => o.BayerId == buyerId);
        if (basket != null)
            await _dbContext.Entry(basket)
                .Collection(i => i.Items).LoadAsync();

        return basket;
    }
}