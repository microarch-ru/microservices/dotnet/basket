﻿using BasketApp.Core.Domain.Model.GoodAggregate;
using BasketApp.Core.Ports;
using Microsoft.EntityFrameworkCore;

namespace BasketApp.Infrastructure.Adapters.Postgres.Repositories;

public class GoodRepository : IGoodRepository
{
    private readonly ApplicationDbContext _dbContext;

    public GoodRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public async Task AddAsync(Good good)
    {
        await _dbContext.Goods.AddAsync(good);
    }

    public void Update(Good good)
    {
        _dbContext.Goods.Update(good);
    }

    public async Task<Good> GetAsync(Guid goodId)
    {
        var good = await _dbContext
            .Goods
            .SingleOrDefaultAsync(o => o.Id == goodId);
        return good;
    }
}