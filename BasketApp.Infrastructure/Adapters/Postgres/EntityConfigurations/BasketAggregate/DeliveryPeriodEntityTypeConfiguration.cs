﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BasketApp.Infrastructure.Adapters.Postgres.EntityConfigurations.BasketAggregate;

internal class DeliveryPeriodEntityTypeConfiguration : IEntityTypeConfiguration<DeliveryPeriod>
{
    public void Configure(EntityTypeBuilder<DeliveryPeriod> entityTypeBuilder)
    {
        entityTypeBuilder
            .ToTable("delivery_periods");

        entityTypeBuilder
            .HasKey(entity => entity.Id);

        entityTypeBuilder
            .Property(entity => entity.Id)
            .ValueGeneratedNever()
            .HasColumnName("id");

        entityTypeBuilder
            .Property(entity => entity.Name)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("name")
            .IsRequired();

        entityTypeBuilder
            .Property(entity => entity.From)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("from")
            .IsRequired();

        entityTypeBuilder
            .Property(entity => entity.To)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("to")
            .IsRequired();
    }
}