﻿using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Domain.Model.GoodAggregate;
using BasketApp.Infrastructure.Adapters.Postgres.Entities;
using BasketApp.Infrastructure.Adapters.Postgres.EntityConfigurations.BasketAggregate;
using BasketApp.Infrastructure.Adapters.Postgres.EntityConfigurations.GoodAggregate;
using BasketApp.Infrastructure.Adapters.Postgres.EntityConfigurations.Outbox;
using Microsoft.EntityFrameworkCore;

namespace BasketApp.Infrastructure.Adapters.Postgres;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    public DbSet<Basket> Baskets { get; set; }
    public DbSet<Good> Goods { get; set; }

    public DbSet<OutboxMessage> OutboxMessages { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Apply Configuration
        modelBuilder.ApplyConfiguration(new BasketEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new ItemEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new DeliveryPeriodEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new GoodEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new OutboxEntityTypeConfiguration());

        // Seed
        modelBuilder.Entity<DeliveryPeriod>(b =>
        {
            var allDeliveryPeriods = DeliveryPeriod.List();
            b.HasData(allDeliveryPeriods.Select(c => new { c.Id, c.Name, c.From, c.To }));
        });

        modelBuilder.Entity<Good>(b =>
        {
            var allGoods = Good.List().ToList();
            b.HasData(allGoods.Select(c => new { c.Id, c.Title, c.Description, c.Price, c.Quantity }));
            b.OwnsOne(e => e.Weight).HasData(allGoods.Select(c => new { GoodId = c.Id, c.Weight.Value }));
        });
    }
}