﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace BasketApp.Infrastructure.Adapters.Postgres.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "delivery_periods",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    from = table.Column<int>(type: "integer", nullable: false),
                    to = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_delivery_periods", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "goods",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    title = table.Column<string>(type: "text", nullable: false),
                    description = table.Column<string>(type: "text", nullable: false),
                    price = table.Column<decimal>(type: "numeric", nullable: false),
                    weight = table.Column<int>(type: "integer", nullable: true),
                    quantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "outbox",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    type = table.Column<string>(type: "text", nullable: false),
                    content = table.Column<string>(type: "text", nullable: false),
                    occurred_on_utc = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    processed_on_utc = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_outbox", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "baskets",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    BayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    address_country = table.Column<string>(type: "text", nullable: true),
                    address_city = table.Column<string>(type: "text", nullable: true),
                    address_street = table.Column<string>(type: "text", nullable: true),
                    address_house = table.Column<string>(type: "text", nullable: true),
                    address_apartment = table.Column<string>(type: "text", nullable: true),
                    delivery_period_id = table.Column<int>(type: "integer", nullable: true),
                    status = table.Column<string>(type: "text", nullable: true),
                    total = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_baskets", x => x.id);
                    table.ForeignKey(
                        name: "FK_baskets_delivery_periods_delivery_period_id",
                        column: x => x.delivery_period_id,
                        principalTable: "delivery_periods",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "items",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    good_id = table.Column<Guid>(type: "uuid", nullable: false),
                    quantity = table.Column<int>(type: "integer", nullable: false),
                    title = table.Column<string>(type: "text", nullable: false),
                    description = table.Column<string>(type: "text", nullable: false),
                    price = table.Column<decimal>(type: "numeric", nullable: false),
                    basket_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_items", x => x.id);
                    table.ForeignKey(
                        name: "FK_items_baskets_basket_id",
                        column: x => x.basket_id,
                        principalTable: "baskets",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "delivery_periods",
                columns: new[] { "id", "from", "name", "to" },
                values: new object[,]
                {
                    { 1, 6, "morning", 12 },
                    { 2, 12, "midday", 17 },
                    { 3, 17, "evening", 24 },
                    { 4, 0, "night", 6 }
                });

            migrationBuilder.InsertData(
                table: "goods",
                columns: new[] { "id", "description", "price", "quantity", "title", "weight" },
                values: new object[,]
                {
                    { new Guid("292dc3c5-2bdd-4e0c-bd75-c5e8b07a8792"), "Описание кофе", 500m, 10, "Кофе", 7 },
                    { new Guid("34b1e64a-6471-44a0-8c4a-e5d21584a76c"), "Описание колбасы", 400m, 10, "Колбаса", 4 },
                    { new Guid("a1d48be9-4c98-4371-97c0-064bde03874e"), "Описание яиц", 300m, 10, "Яйца", 8 },
                    { new Guid("a3fcc8e1-d2a3-4bd6-9421-c82019e21c2d"), "Описание сахара", 600m, 10, "Сахар", 1 },
                    { new Guid("e8cb8a0b-d302-485a-801c-5fb50aced4d5"), "Описание молока", 200m, 10, "Молоко", 9 },
                    { new Guid("ec85ceee-f186-4e9c-a4dd-2929e69e586c"), "Описание хлеба", 100m, 10, "Хлеб", 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_baskets_delivery_period_id",
                table: "baskets",
                column: "delivery_period_id");

            migrationBuilder.CreateIndex(
                name: "IX_items_basket_id",
                table: "items",
                column: "basket_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "goods");

            migrationBuilder.DropTable(
                name: "items");

            migrationBuilder.DropTable(
                name: "outbox");

            migrationBuilder.DropTable(
                name: "baskets");

            migrationBuilder.DropTable(
                name: "delivery_periods");
        }
    }
}
