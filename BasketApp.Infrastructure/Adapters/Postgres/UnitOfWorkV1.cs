﻿using Primitives;

namespace BasketApp.Infrastructure.Adapters.Postgres;

public class UnitOfWorkV1 : IUnitOfWork, IDisposable
{
    private readonly ApplicationDbContext _dbContext;

    private bool _disposed;

    public UnitOfWorkV1(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public async Task<bool> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        await _dbContext.SaveChangesAsync(cancellationToken);
        return true;
    }

    private void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing) _dbContext.Dispose();
            _disposed = true;
        }
    }
}