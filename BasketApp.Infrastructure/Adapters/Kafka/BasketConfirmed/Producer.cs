using BasketApp.Core.Domain.Model.BasketAggregate.DomainEvents;
using BasketApp.Core.Ports;
using BasketConfirmed;
using Confluent.Kafka;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace BasketApp.Infrastructure.Adapters.Kafka.BasketConfirmed;

/// <summary>
///     Producer для Kafka
/// </summary>
public sealed class Producer : IMessageBusProducer
{
    private readonly ProducerConfig _config;
    private readonly string _topicName;

    public Producer(IOptions<Settings> options)
    {
        if (string.IsNullOrWhiteSpace(options.Value.MessageBrokerHost)) throw new ArgumentException(nameof(options.Value.MessageBrokerHost));
        if (string.IsNullOrWhiteSpace(options.Value.BasketConfirmedTopic)) throw new ArgumentException(nameof(options.Value.BasketConfirmedTopic));

        _config = new ProducerConfig
        {
            BootstrapServers = options.Value.MessageBrokerHost
        };
        _topicName = options.Value.BasketConfirmedTopic;
    }

    public async Task Publish(BasketConfirmedDomainEvent notification, CancellationToken cancellationToken)
    {
        // Перекладываем данные из Domain Event в Integration Event
        var basketConfirmedIntegrationEvent = new BasketConfirmedIntegrationEvent
        {
            BasketId = notification.Basket.Id.ToString(),
            Address = new Address
            {
                Country = notification.Basket.Address.Country,
                City = notification.Basket.Address.City,
                Street = notification.Basket.Address.Street,
                House = notification.Basket.Address.House,
                Apartment = notification.Basket.Address.Apartment
            },
            DeliveryPeriod = new DeliveryPeriod
            {
                From = notification.Basket.DeliveryPeriod.From,
                To = notification.Basket.DeliveryPeriod.To
            },
            Items =
            {
                notification.Basket.Items.Select(c => new Item
                {
                    Id = c.Id.ToString(),
                    GoodId = c.GoodId.ToString(),
                    Title = c.Title,
                    Price = (double)c.Price,
                    Quantity = c.Quantity
                })
            }
        };

        // Создаем сообщение для Kafka
        var message = new Message<string, string>
        {
            Key = notification.EventId.ToString(),
            Value = JsonConvert.SerializeObject(basketConfirmedIntegrationEvent)
        };

        try
        {
            // Отправляем сообщение в Kafka
            using var producer = new ProducerBuilder<string, string>(_config).Build();
            var dr = await producer.ProduceAsync(_topicName, message, cancellationToken);
            Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
        }
        catch (ProduceException<Null, string> e)
        {
            Console.WriteLine($"Delivery failed: {e.Error.Reason}");
        }
    }
}