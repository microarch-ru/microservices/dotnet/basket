using BasketApp.Core.Domain.Model.BasketAggregate;
using BasketApp.Core.Ports;
using CSharpFunctionalExtensions;
using DiscountApp.Api;
using Grpc.Core;
using Grpc.Net.Client;
using Grpc.Net.Client.Configuration;
using Microsoft.Extensions.Options;
using Primitives;
using Discount = BasketApp.Core.Domain.Model.SharedKernel.Discount;
using Item = DiscountApp.Api.Item;

namespace BasketApp.Infrastructure.Adapters.Grpc.DiscountService;

public class Client : IDiscountClient
{
    private readonly MethodConfig _methodConfig;
    private readonly SocketsHttpHandler _socketsHttpHandler;
    private readonly string _url;

    public Client(IOptions<Settings> options)
    {
        if (string.IsNullOrWhiteSpace(options.Value.DiscountServiceGrpcHost)) 
            throw new ArgumentException(nameof(options.Value.DiscountServiceGrpcHost));
        _url = options.Value.DiscountServiceGrpcHost;

        _socketsHttpHandler = new SocketsHttpHandler
        {
            PooledConnectionIdleTimeout = Timeout.InfiniteTimeSpan,
            KeepAlivePingDelay = TimeSpan.FromSeconds(60),
            KeepAlivePingTimeout = TimeSpan.FromSeconds(30),
            EnableMultipleHttp2Connections = true
        };

        _methodConfig = new MethodConfig
        {
            Names = { MethodName.Default },
            RetryPolicy = new RetryPolicy
            {
                MaxAttempts = 5,
                InitialBackoff = TimeSpan.FromSeconds(1),
                MaxBackoff = TimeSpan.FromSeconds(5),
                BackoffMultiplier = 1.5,
                RetryableStatusCodes = { StatusCode.Unavailable }
            }
        };
    }

    public async Task<Result<Discount, Error>> GetDiscountAsync(Basket basket, CancellationToken cancellationToken)
    {
        using var channel = GrpcChannel.ForAddress(_url, new GrpcChannelOptions
        {
            HttpHandler = _socketsHttpHandler,
            ServiceConfig = new ServiceConfig { MethodConfigs = { _methodConfig } }
        });

        var client = new DiscountApp.Api.Discount.DiscountClient(channel);
        var reply = await client.GetDiscountAsync(new GetDiscountRequest
        {
            Items =
            {
                basket.Items.Select(c => new Item
                {
                    Id = c.Id.ToString()
                })
            }
        }, null, DateTime.UtcNow.AddSeconds(5), cancellationToken);

        var result = Discount.Create(reply.Value);
        return result;
    }
}